﻿using Domain.Entity;
using Domain.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;

using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebUI.ViewModels;
using WebUI.ViewModels.Interfaces;

namespace WebUI.Controllers
{
    [Authorize]
    public class ExamController : Controller
    {

        private EfDbContext _db = new EfDbContext();
        // GET: Exam
        public ActionResult Index()
        {            
            return View();
        }

        [Authorize(Roles = "Преподаватель"),HttpGet]
        public ActionResult Create()
        {

            ViewBag.Category = new SelectList(_db.TestCategories.ToList(), "Id", "Name");
            return View();
        }

        [Authorize(Roles ="Преподаватель"), HttpPost]
        public ActionResult Create(TestViewModel testVm)
        {
            if (ModelState.IsValid)
            {
                Test test = testVm.ConvertToTest();
                test.TeacherId = User.Identity.GetUserId();
                try
                {
                    _db.Test.Add(test);
                    _db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    ViewBag.ErrorMessage = e.Message;
                    ViewBag.Category = new SelectList(_db.TestCategories.ToList(), "Id", "Name");
                    return View(testVm);
                }
                return RedirectToAction("Details", "Cabinet", new {id = test.Id});
            }
            ViewBag.Category = new SelectList(_db.TestCategories.ToList(), "Id", "Name");

            return View(testVm);

        }

        [Authorize(Roles = "Преподаватель"), HttpGet]
        public ActionResult Edit(int Id)
        {
            var selectedTest = _db.Test.Find(Id);
            ITestCreate model = new TestViewModel(selectedTest);
            if (selectedTest == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View(model);
        }

        [Authorize(Roles = "Преподаватель"), HttpPost]
        public ActionResult Edit(TestViewModel testVm)
        {
            var selectedTest = testVm.ConvertToTest();
            try
            {
                selectedTest.TeacherId = User.Identity.GetUserId();
                _db.Entry(selectedTest).State = EntityState.Modified;
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return View(testVm);
            }
            return RedirectToAction("TestDetails", "Cabinet", new { TestId = selectedTest.Id });
        }

        [Authorize(Roles = "Преподаватель")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userId = User.Identity.GetUserId();
            var test = _db.Test.Find(id);
            if (test == null)
            {
                return HttpNotFound();
            }
            if ( userId != test.TeacherId)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }    

            ITestCreate model = new TestViewModel(test);
            return PartialView(model);
        }

        // POST: Faculties/Delete/5
        [HttpPost, ActionName("Delete"), Authorize(Roles = "Преподаватель")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var test = _db.Test.Find(id);
            _db.Test.Remove(test);
            _db.SaveChanges();
            return RedirectToAction("ShowTests", "Cabinet");
        }

        [Authorize(Roles = "Преподаватель"), HttpGet]

        public ActionResult AddGroups(int Id=0)
        {
            if(Id==0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var groups = _db.Groups.ToList();
            var test = _db.Test.Find(Id);
            var passedGroupsNum = test?.PassedGroups;
            if (!groups.Any()|| passedGroupsNum==null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "В БД отсутствуют записи о группах!");

            var selected = passedGroupsNum.Select(g => g.Id).ToList();     

            ViewBag.GropList = new MultiSelectList(groups, "Id", "Num", selected);
            ViewBag.TestName = test.Name;
            ViewBag.IsOpenChecked = test.IsOpenTest;
            var model = new PassingGroupsVm{TestId = Id, GropList = selected.ToArray()};

            return View(model);

        }

        [Authorize(Roles = "Преподаватель"), HttpPost]
        // public ActionResult AddGroups(PassingGroupsVm model,int[] GropList)
        public ActionResult AddGroups(PassingGroupsVm model)
        {
            var test = _db.Test.Find(model.TestId);
            if(test==null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (model.IsOpen == "on")//тест открыт и виден всем желающим
            {
                test.IsOpenTest = true;
                test.PassedGroups.Clear();
            }
            else//тест либо закрыт, либо ограничен
            {
                test.IsOpenTest = false;
                if (model.GropList != null)
                {
                    foreach (var groupId in model.GropList)
                    {
                        test.PassedGroups.Add(_db.Groups.Find(groupId));
                    }
                }
            }
            _db.SaveChanges();

            //var listOfGroups = new List<Groups>();
            //if(model.GropList==null)//если удалили все группы из списка
            //{
            //    test.PassedGroups.Clear();
            //}
            //else
            //{
            //    foreach (var groupId in model.GropList)
            //    {
            //        var d = _db.Groups.Find(groupId);
            //        if (d != null) listOfGroups.Add(d);
            //    }

            //    test.PassedGroups.Clear();
            //    test.PassedGroups = listOfGroups;     
            //}

           // _db.Entry(test).State = EntityState.Modified;
           // _db.SaveChanges();

            return RedirectToAction("Index", "Cabinet");

            //var groups = _db.Groups.ToList();
            //if (groups == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Сущность не найдена в БД");
            //ViewBag.GropList = new SelectList(groups, "Id", "Num");
            //return View(model);
        }
        
    }
}