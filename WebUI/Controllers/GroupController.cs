﻿using Domain.Abstract;
using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    [Authorize(Roles = "Администратор, Преподаватель")]
    public class GroupController : Controller
    {
        private ITemplateRepository<Groups> _repo;
        public GroupController(ITemplateRepository<Groups> repository)
        {
            _repo = repository;
        }
        // GET: Group
        public ActionResult Index()
        {
            return View(_repo.ItemCollection);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Groups group)
        {
            if (ModelState.IsValid)
            {
                if (_repo.ItemCollection.FirstOrDefault(g => g.Num == group.Num) != null)
                {
                    ModelState.AddModelError("Num", @"Группа с таким номером уже зарегистрирована");
                    return View(group);
                }
                try
                {
                    _repo.Create(group);
                }
                catch (Exception)
                {

                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                return RedirectToAction("Index");
            }
            return View(group);
        }

        public ActionResult Delete(int Id=0)
        {
            if(Id==0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var item = _repo.Details(Id);
            if (item != null)
            {
                _repo.Delete(item);
            }
                return RedirectToAction("Index");
        }


        public ActionResult Details(int Id=0)
        {
            if (Id == 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var model = _repo.Details(Id);
            if(model!=null)
            return View(model);
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }
    }
}