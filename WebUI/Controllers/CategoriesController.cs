﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entity;

namespace WebUI.Controllers
{
    public class CategoriesController : Controller
    {
        private EfDbContext _db = new EfDbContext();
        // GET: Categories
        public ActionResult Index()
        {
            return View(_db.TestCategories.ToList());
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(TestCategory category)
        {
            if (ModelState.IsValid)
            {
                _db.TestCategories.Add(category);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(category);

        }

        //удаление через аjax
        public ActionResult Delete(int id = 0)
        {
            if (id == 0)
            {
                Response.StatusCode = 400;
                return null;
            }

            var item = _db.TestCategories.Find(id);
            if (item != null)
            {
                _db.TestCategories.Remove(item);
                _db.SaveChanges();
                Response.StatusCode = 200;

                return Json(new {message = "success"},JsonRequestBehavior.AllowGet);
            }
            Response.StatusCode = 400;
            return Json(new {message = "error"},JsonRequestBehavior.AllowGet);
        }
    }
}