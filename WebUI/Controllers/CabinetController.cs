﻿using Domain.Entity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.ViewModels;
using Microsoft.AspNet.Identity;
using WebUI.ViewModels.Interfaces;

namespace WebUI.Controllers
{
    [Authorize(Roles ="Преподаватель")]
    public class CabinetController : Controller
    {
        private EfDbContext _db = new EfDbContext();
        private ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        // GET: Cabinet
        [Authorize(Roles = "Преподаватель")]
        public async Task<ActionResult> Index()
        {
            //вернуть список тестов, созданных данным преподавателем
            var user = User.Identity.GetUserId();
            var testsFromCurrentUser = await _db.Test.Where(t => t.TeacherId == user).ToListAsync();

            var model = new List<TestViewModel>();
            foreach (var test in testsFromCurrentUser)
            {
                model.Add(new TestViewModel(test));
            }

           // ViewBag.TeacherFio = UserManager.FindByName(User.Identity.Name).Fio;

            return View(model);
        }

        //список вопросов/ответов конкретного теста


        //------------TEST------------//
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(TestViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Test test = model.ConvertToTest();
            test.TeacherId = User.Identity.GetUserId();
            try
            {
                _db.Test.Add(test);
                _db.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                ViewBag.ErrorMessage = e.Message;
                return View(model);
            }

            return RedirectToAction("Details", new { id = test.Id });
        }


        public ActionResult Details(int id)
        {
            var test = _db.Test.Find(id);
            if (test == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ITestDetails model = new TestViewModel(test);

            return View(model);
        }
        //------------ENDTEST------------//

        //------------TEST------------//
        [HttpGet]
        public ActionResult EditQuestions(int Id)
        {
            return View();
        }

        [HttpPost]
        public JsonResult EditQuestions(QuestionViewModel question)
        {
            if (ModelState.IsValid)
            {
                _db.Questions.Add(question.ConvertToQuestion());
                _db.SaveChanges();
            }
            return Json(question, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllQuestion(int id)
        {
            var questions = _db.Questions.Where(q => q.TestId == id);
            return Json(questions.Select(s => new { text = s.Question, comment = s.Comment, id = s.Id }), JsonRequestBehavior.AllowGet);
        }
        //id- questionID
        public JsonResult GetAnswers(int id)
        {
            var answers = _db.Answers.Where(a => a.QuestionId == id);
            return Json(answers.Select(a => new { id = a.Id, text = a.Text, isTrueAnswer = a.IsTrueAnswer }),
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult AddAnswerForm(int id)
        {
            var quest = _db.Questions.Find(id);
            if (quest != null)
            {
                ViewBag.Question = quest.Question;
                return PartialView("_AddAnswer", new AnsverViewModel { QuestionId = id });
            }

            return PartialView(null);
        }

        [HttpPost]
        public ActionResult AddAnswer(AnsverViewModel model)
        {
            model.IsTrueAnswer = AnsverViewModel.ConvertCheckResultToBool(model.TrueAnswer);
            if (ModelState.IsValid)
            {
                var answer = model.ConvertToAnswer();
                _db.Answers.Add(answer);
                _db.SaveChanges();

                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { text = answer.Text, isTrueAnswer = answer.IsTrueAnswer, id = answer.Id, message = "Ответ успешно добавлен" }, JsonRequestBehavior.AllowGet);
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(ModelState.Values.SelectMany(m => m.Errors).Select(m => m.ErrorMessage).ToList(), MediaTypeNames.Text.Plain);
        }

        public JsonResult DeleteAnswer(int id)
        {
            var question = _db.Answers.Find(id);

            if (question != null)
            {
                int parentId = question.QuestionId;
                _db.Answers.Remove(question);
                _db.SaveChanges();
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { Id = parentId, message = "Успешно удалено" }, JsonRequestBehavior.AllowGet);

            }
            Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return Json(new { message = "Ошибка" }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        //------------QUESTION------------//
        public ActionResult AddQuestionForm(int id)
        {
            var quest = _db.Test.Find(id);
            if (quest != null)
            {
                ViewBag.Question = quest.Questions;
                return PartialView("_addQuestion", new QuestionViewModel { TestId = id });
            }

            return PartialView(null);

        }
        [HttpPost]
        public JsonResult AddQuestion(QuestionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var data = model.ConvertToQuestion();
                _db.Questions.Add(data);
                _db.SaveChanges();

                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { text = data.Question, comment = data.Comment, id = data.Id, message = "Вопрос добавлен" },
                    JsonRequestBehavior.AllowGet);
            }
            Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return Json(new { message = "Ошибка" });
        }

        public JsonResult DeleteQuestion(int id)
        {
            var question = _db.Questions.Find(id);
            if (question != null)
            {
                _db.Questions.Remove(question);
                _db.SaveChanges();
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { id, message = "Успешно удалено" }, JsonRequestBehavior.AllowGet);
            }
            Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return Json(new { id, message = "Ошибка" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AnswersCount(int id)
        {
            return Json(new { count = _db.Answers.Count(a => a.QuestionId == id) });
        }

        [HttpPost]
        public JsonResult IsOpenTest(int TestId, string IsOpenTest)
        //public JsonResult IsOpenTest()
        {
            var test = _db.Test.Find(TestId);
            string message = "Ошибка";
            string record = null;
            if (test != null && test.TeacherId == User.Identity.GetUserId())
            {
               
                if (IsOpenTest == "on")
                {
                    test.IsOpenTest =true ;
                    message = "Доступ открыт";
                    record = "Доступен к прохождению";
                }
                else
                {
                    test.IsOpenTest = false;
                    message = "Доступ ограничен";
                    record = "Закрыт";
                }
            }
            _db.SaveChanges();

            return Json(new {message, record});
        }


    }
}