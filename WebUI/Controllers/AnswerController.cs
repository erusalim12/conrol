﻿using Domain.Entity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebUI.ViewModels;

namespace WebUI.Controllers
{
    public class AnswerController : Controller
    {
        private EfDbContext _db = new EfDbContext();

        //id = questionId
        [HttpGet]
        public ActionResult Create(int Id)
        {
            var question = _db.Questions.Find(Id);
            ViewBag.Question = question.Question;
            var model = new AnsverViewModel { QuestionId = Id};
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(AnsverViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var answer = new Answers
            {
                IsTrueAnswer = model.IsTrueAnswer,
                Text = model.Text,
                QuestionId = model.QuestionId
            };

            try
            {
                _db.Answers.Add(answer);
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return View(model);
            }
            return RedirectToAction("Details", "Questions", new { Id = answer.QuestionId });
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var currentAnswer = _db.Answers.Find(Id);
            if (currentAnswer == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                if (currentAnswer.Questions.Test.TeacherId != User.Identity.GetUserId())
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var model = new AnsverViewModel(currentAnswer);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(AnsverViewModel model)
        {
       
            var answer1 = _db.Answers.Find( model.Id);
            if (answer1 == null) return View(model);

            try
            {
                answer1.IsTrueAnswer = model.IsTrueAnswer;
                answer1.Text = model.Text;
                
                _db.SaveChanges();
            }
            catch (Exception )
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return RedirectToAction("Details", "Questions", new {Id = model.QuestionId});
        }   

        // POST: Faculties/Delete/5
        [Authorize(Roles = "Преподаватель")]
        public ActionResult Delete(int Id)
        {
            if (Id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var answer = _db.Answers.Find(Id);
            try
            {
                if (IdentityExtensions.GetUserId(User.Identity) != answer.Questions.Test.TeacherId)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Please, try later...or never");
            }


            if (answer == null)
            {
                return HttpNotFound();
            }

            var questionId = answer.Questions.Id;
            _db.Answers.Remove(answer);
            _db.SaveChanges();
            return RedirectToAction("Details", "Questions", new { Id = questionId });
        }
    }
}