﻿using Domain.Entity;
using Microsoft.AspNet.Identity;
using System;
using System.Net;
using System.Web.Mvc;
using WebUI.ViewModels;

namespace WebUI.Controllers
{
    [Authorize(Roles ="Преподаватель")]
    public class QuestionsController : Controller
    {
        private EfDbContext _db = new EfDbContext();

        //id-вопроса, для которого запиливаются ответы
        [HttpGet, Authorize(Roles = "Преподаватель")]
        public ActionResult Create(int Id)
        {
            IQuestion model = new QuestionViewModel
            {
                TestId = Id
            };
            return View(model);
        }

        public ActionResult Details(int Id)
        {
          var question=  _db.Questions.Find(Id);
            if (question == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (question.Test.TeacherId !=User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            return View(new QuestionViewModel(question));

        }


        public ActionResult Create(QuestionViewModel model)
        {
            var question = new Questions
            {
                TestId = model.TestId,
                Question = model.Question,
                Comment = model.Comment
            };
            //сохраняем вопрос
            try
            {
                _db.Questions.Add(question);
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return View(model);
            }
            return RedirectToAction("Details", "Questions", new {Id = question.Id });
        }

        [HttpGet]
        [Authorize(Roles ="Преподаватель")]
        public ActionResult Edit(int Id)
        {
            var quest = _db.Questions.Find(Id);
            if (quest == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (User.Identity.GetUserId() != quest.Test.TeacherId)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            IQuestion question = new QuestionViewModel {Id = Id };
            return View(question);
        }

        [HttpPost]
        [Authorize(Roles = "Преподаватель")]
        public ActionResult Edit(QuestionViewModel question)
        {
            var selectedQuest = _db.Questions.Find(question.Id);
            if (selectedQuest != null)
            {
                selectedQuest.Question = question.Question;
                selectedQuest.Comment = question.Comment;
            }
            else
            {
                ModelState.AddModelError("id",@"Ошибка с редактированием вопроса");
            }
            if (!ModelState.IsValid) return View(question);
            try
            {
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            return RedirectToAction("Create", "Answer",new {Id = question.Id });
        }

        [Authorize(Roles = "Преподаватель")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
         
            var questions = _db.Questions.Find(id);
            if (questions == null)
            {
                return HttpNotFound();
            }

            try
            {
                if (User.Identity.GetUserId() != questions.Test.TeacherId)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest,"Please, try later...or never");
            }
           
            IQuestion model = new QuestionViewModel(questions);
            return PartialView(model);
        }

        // POST: Faculties/Delete/5
        [HttpPost, ActionName("Delete"), Authorize(Roles = "Преподаватель")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var quest = _db.Questions.Find(id);
            _db.Questions.Remove(quest ?? throw new InvalidOperationException());
            _db.SaveChanges();
            return RedirectToAction("ShowTests", "Cabinet",quest.TestId);
        }


    }
}