﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entity;
using Domain.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using WebUI.ViewModels;

namespace WebUI.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
        private ApplicationRoleManager RoleManager => HttpContext.GetOwinContext().Get<ApplicationRoleManager>();

        private EfDbContext _db = new EfDbContext();
       
        public ActionResult Index()
        {
            //отобразить список доступных к прохождению тестов
            List<Test> allowedTests;

            string userId;
            if (User.IsInRole("Преподаватель")|| User.IsInRole("Администратор")|| User.IsInRole("Модератор"))
            {
                //преподаватель видит все открытые тесты, а так-же  свои
                //var userId = User.Identity.GetUserId();
                //allowedTests = _db.Test.Where(t => t.IsOpenTest||t.TeacherId== userId).ToList();
                //преподаватель видит все открытые тесты
                //он не видит закрытые тесты с доступом для групп, он не видит закрытые
                allowedTests = _db.Test.Where(t=>t.IsOpenTest).ToList();
                
            }
            else
            {
                //курсант видит все тесты для его группы, а так-же все открытые
                int groupNum;
                using (var apContext = new ApplicationContext())
                {
                    userId = User.Identity.GetUserId();
                    groupNum = apContext.Users.First(u => u.Id == userId).GroupNum.Value;
                }
                 allowedTests = _db.Test.Where(t => t.PassedGroups.Contains(_db.Groups.FirstOrDefault(g => g.Num == groupNum))||t.IsOpenTest).ToList();
                if (allowedTests.Count == 0)
                {
                    return View();
                }
            }

            var model = new List<IShowTests>();
            userId = User.Identity.GetUserId();
            
            foreach (var test in allowedTests)
            {
                TestViewModel item = new TestViewModel(test);

                var result = _db.Result.FirstOrDefault(r => r.UserId == userId&&r.TestId == test.Id);
                if (result != null)
                {
                    item.IsPassed = true;
                }
                model.Add(item);
            }

            return View(model);
        }
        
        public ActionResult _UserRoles()
        {
            IList<string> roles = new List<string> { "Роль не определена" };
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
                .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            if (user != null)
                roles = userManager.GetRoles(user.Id);
            return PartialView(roles);
        }

    }
}