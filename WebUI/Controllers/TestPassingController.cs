﻿using Domain.Entity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.ViewModels;
using static WebUI.ViewModels.Quiz;


namespace WebUI.Controllers
{
    [Authorize]
    public class TestPassingController : Controller
    {
        EfDbContext _context = new EfDbContext();
        // GET: TestPassing
        [HttpGet]
        public ActionResult Passing(int? id)
        {
            if(id==null) return new HttpStatusCodeResult( HttpStatusCode.BadRequest);

            var quiz = _context.Test.Find(id.Value);

            if (quiz != null)
            {
                quiz.Questions = (ICollection<Questions>) FetchedQuestions(quiz.Questions.ToArray(),quiz.FetchSize);
                return View(quiz);
            }
            return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
        }

        private static readonly Random Random = new Random(DateTime.Now.Millisecond);


        /// <summary>
        /// Возвращает N-случайных вопросов теста
        /// </summary>
        private IEnumerable<Questions> FetchedQuestions(Questions[] questions,int fetchSize)
        {
            //если размер выборки больше чем кол-во вопросов теста - вернем весь список вопросов
            if (fetchSize >= questions.Length)
            {
                return questions.ToList();
            }
            //а теперь надо выбрать случайные вопросы из теста и вставить их в случайном порядке
            Questions[] model = new Questions[questions.Length];

          //выбирает первые N 
            for (var i = 0; i < questions.Length; i++)
            {
                var j = Random.Next(i + 1);
                if (j != i)
                    model[i] = model[j];
                    model[j] = questions.ToArray()[i];
            }

            return model.Take(fetchSize).ToList();
        }

        [HttpPost]
        public JsonResult Passing(QuizAnswers data)
        {
            if (data == null || data.QuizID == 0)
            {
                Response.StatusCode = new HttpStatusCodeResult(HttpStatusCode.BadRequest).StatusCode;
                return Json("error",JsonRequestBehavior.AllowGet);
            }

                //этапы:
                    //1 - подсчитать количество правильных ответов на тест
                    //3 - сохранить результаты прохождения в базу
                    //редирект на ActionResult Result с ID == id результата текущего пользователя
                var result = DetermingResults(data);

                var test = _context.Test.Find(data.QuizID);
                TimeSpan timeToExec =  test?.TimeToExecute ?? TimeSpan.MinValue;
                var resultModel = new Result
                {
                    Mark = result.Mark,
                    TestId = data.QuizID ,
                    QuestionCount = result.QuestCount,
                    ResultInPercent = result.ResultInPercent,
                    RightAnswerCount = result.RigthAnswers,
                    TimeToFill = timeToExec,
                    UserId = User.Identity.GetUserId()
                };

                resultModel.SpentTime = resultModel.TimeToFill - ConvertStringToTimeSpan(data.FilledTime);

                _context.Result.Add(resultModel);
                _context.SaveChanges();

            Response.StatusCode = new HttpStatusCodeResult(HttpStatusCode.OK).StatusCode;
            Session["Result"] = data.Answers.Select(quizAnswer => new Results
            {
                QuestionId = quizAnswer.QuestionId,
                AnswersId = quizAnswer.AnswerId
            }).ToList();

            return Json(resultModel.Id, JsonRequestBehavior.AllowGet);
        }

        //отображение результата прохождения теста
        public ActionResult Results(int? Id)
        {
            if (Id == null||Id.Value==0)
            {
                return View();
            }

            var content = new ResultViewModel(_context.Result.Find(Id))
                { Results = (List<Results>) Session["Result"]};

            return View(content);
        }

        [HttpPost]
        //расчет результата теста
        private ExamResult DetermingResults(QuizAnswers data)
        {
            ExamResult result = new ExamResult {QuestCount = (byte) data.Answers.Count};

            //для каждого ответа
            foreach (var answers in data.Answers)
            {
                if (answers.AnswerId != null)
                {
                    //в одном вопросе может быть несколько ответов (в том числе и правильных)
                        //ответ считается верным, если даны все верные и ниодного не верного ответа на вопрос

                    var listOfTrueAnswers = _context.Answers.Where(a => a.QuestionId == answers.QuestionId && a.IsTrueAnswer)
                        .Select(a => a.Id).ToArray();

                    if (listOfTrueAnswers.SequenceEqual(answers.AnswerId))
                        result.RigthAnswers++;
                }
                //result.QuestCount++;
            }
      
            if (result.RigthAnswers != 0)
            {
                result.ResultInPercent =(float) result.RigthAnswers / result.QuestCount * 100;//get result in percents
                result.Mark = GetMark(result.ResultInPercent, data.QuizID);
            }
            else      //если нет ниодного правильного ответа
            {  
                result.ResultInPercent = 0;
                result.Mark = 2;
            }

            return result;
        }
        
       /// <summary>
       /// Выставление оценки в зависимости от успешности прохождения 
       /// </summary>
       /// <param name="result">результат в %</param>
       /// <param name="testId">Id теста</param>
       /// <returns>Оценка в диапазоне от 2(неуд), до 5(отл)</returns>
        private byte GetMark(float result,int testId)
        {
            var test = _context.Test.Find(testId);
            if (test == null) return 0;

            if (result >= test.Perfect)
            {
                return 5;
            }

            if(result>=test.Good && result < test.Perfect)
            {
                return 4;
            }

            if (result >= test.Satisfactorily && result < test.Good)
            {
                return 3;
            }
            return 2;            
        }


        //после прохождения квеста заполняем соотв. поле в бд.
        //возвращаем ID записи для сохранения списка ответов курсанта ?? даже не знаю зачем это надо-то
        //private int SaveTestPassing(string timeCount,int quizId, IList<QuestionAnswer> answers,byte result)
        //{
        //        var testPassing = new CursantTestPassing
        //        {
        //            CursantId = User.Identity.GetUserId(),
        //            PassingDate = DateTime.Now.ToShortDateString(),
        //            TimeCount = ConvertStringToTimeSpan(timeCount),
        //            TestId = quizId,
        //            Result = result
        //        };
        //        _context.CursantTestPassing.Add(testPassing);
        //        _context.SaveChanges();
        //        return testPassing.Id;
        //}
        
        private TimeSpan ConvertStringToTimeSpan(string value)
        {
            //textFormat mm:ss
            if (value == null) return TimeSpan.FromMinutes(0);
            try
            {
                var temp = value.Split(':');
                return TimeSpan.FromSeconds(int.Parse(temp[0]) * 60 + int.Parse(temp[1]));//затраченное время в секундах 
            }
            catch (Exception)
            {
                return TimeSpan.FromMinutes(0);
            }
        }

        public PartialViewResult _detailedResult(int questionId, int[] answersId)
        {
            var question = _context.Questions.Find(questionId);

            if (question == null) return PartialView();

            var model = new DetailedResultVm {Question = question.Question,Comment = question.Comment};
            if(answersId!=null)
            {
                model.CurrentAnswers.AddRange(from answer in question.Answers
                from i in answersId
                where answer.Id == i
                select answer.Text);
            }

            model.RigthAnswers = question.Answers.Where(a => a.IsTrueAnswer).Select(a => a.Text).ToList();
            if (model.RigthAnswers.SequenceEqual(model.CurrentAnswers))
            {
                model.IsTrueAnswer = true;
            }

            return PartialView(model);
        }


        /// <summary>
        /// Возвращает результат предыдущих прохождений тестов
        /// </summary>
        /// <param name="id">Id-Теста</param>
        /// <returns></returns>
        public ActionResult GetResultHistory(int id)
        {
            var user = User.Identity.GetUserId();
            var results = _context.Result.Where(r => r.UserId == user&&r.TestId==id).ToList();

            return View(results);
        }

    }
}