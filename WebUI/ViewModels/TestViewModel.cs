﻿using Domain.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using WebUI.ViewModels.Interfaces;

namespace WebUI.ViewModels
{
    /// <summary>
    /// модель для отображения тесто в личном кабинете преподавателя
    /// </summary>
    public class TestViewModel:ITestDetails, IShowTests
    {
        public TestViewModel()
        {
                
        }
        public TestViewModel(Test test)
        {
            AcceptedGroups = string.Join(", ",test.PassedGroups.Select(g => g.Num).OrderBy(e => e));
            Name = test.Name;
            AddingDate = test.AddingDate;
            TimeToExecute = test.TimeToExecute;
            Id = test.Id;
            TeacherId = test.TeacherId;
            IsOpenTest = test.IsOpenTest;           
            QuestionsCount = test.Questions.Count;
            Questions = FillQuestions(test.Questions);
            Good = test.Good;
            Perfect = test.Perfect;
            Satisfactorily = test.Satisfactorily;
            FetchSize = test.FetchSize;
            CategoryId = test.CategoryId;
            Comment = test.Comment;

            NumsOfPassedGroup = string.Join(", ", test.PassedGroups.Select(g => g.Num).OrderBy(e => e));

        }
        public string Comment { get; set; }
        public  string TeacherName { get; set; }
        public string TeacherId { get; set; }

        public int QuestionsCount { get; set; }

        [Display(Name = "Дата добавления")]
        public string AddingDate { get; set; }

        public List<QuestionViewModel> Questions { get; set; }

        public string NumsOfPassedGroup { get; set; }

        [Display(Name = "Допущенные группы")]
        public string AcceptedGroups { get; set; }

        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Время на выполнение"), Required]
         [RegularExpression(@"((([0-1][0-9])|(2[0-3]))(:[0-5][0-9])(:[0-5][0-9])?)", ErrorMessage = "Минимальное время на выполнение теста - 10 секунд!")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = @"{0:mm\:ss}")]
        public TimeSpan TimeToExecute { get; set; }

        public bool IsPassed { get; set; }

        [Display(Name = "Открытый тест")]
        public bool IsOpenTest { get; set; }



        [Required]
        [Range(0, 100)]
        public byte Perfect { get; set; }

        [Range(0, 100)]
        public byte Good { get; set; }
      
        [Range(0, 100)]
        public byte Satisfactorily { get; set; }

        [Display(Name ="Количество вопросов в тестовом наборе")]
        public byte FetchSize { get; set; }
        [Display(Name = "Категория")]
        public int CategoryId { get; set; }
        public Test ConvertToTest()
        {
            Test t = new Test
            {
                Id = Id,
                Good = Good,
                Perfect = Perfect,
                Satisfactorily = Satisfactorily,
                Name = Name,
                IsOpenTest = IsOpenTest,
                TimeToExecute = TimeToExecute,
                FetchSize = FetchSize,
                CategoryId = CategoryId,
                Comment =  Comment
            };
            return t;
        }

    

        private List<QuestionViewModel> FillQuestions(IEnumerable<Questions> questions)
        {
            var collection = from question in questions
                select question;

            return collection.Select(item => new QuestionViewModel(item)).ToList();
        }
    }
}