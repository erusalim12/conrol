﻿using Domain.Entity;
using System.Collections.Generic;
using System.Linq;

namespace WebUI.ViewModels
{
    //вопросы
    public class QuestionViewModel: IQuestion
    {

        public QuestionViewModel(Questions question)
        {
            Id = question.Id;
            Text = question.Question;
            TestId = question.TestId;
            //Answers = ConvertAnswersToAnsverViewModels(question.Answers);
            Comment = question.Comment;
        }

        public QuestionViewModel()
        {
            
        }
        public int Id { get; set; }
        public string Text { get; set;}
        public string Question { get; set; }
        public string Comment { get; set; }
        public int TestId { get; set; }
        public ICollection<Answers> Answers { get; set; }

        private List<AnsverViewModel> ConvertAnswersToAnsverViewModels(IEnumerable<Answers> answers)
        {
            var collection = from answer in answers
                             select answer;

            List<AnsverViewModel> avm = new List<AnsverViewModel>();
            foreach (var item in collection)
            {
                avm.Add(new AnsverViewModel(item));
            }
            return avm;
        }

        public Questions ConvertToQuestion()
        {
            return new Questions
            {
                Question = Question,
                Comment =  Comment,
                TestId = TestId,
                Id = Id,
                Answers =  Answers
            };
        }
    }

}