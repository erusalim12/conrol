﻿using System.ComponentModel.DataAnnotations;
using Domain.Entity;

namespace WebUI.ViewModels
{
    //ответы
    public class AnsverViewModel
    {
        public AnsverViewModel(Answers answer)
        {
            Id = answer.Id;
            Text = answer.Text;
            IsTrueAnswer = answer.IsTrueAnswer;
            QuestionId = answer.QuestionId;
        }
        public AnsverViewModel()
        {

        }
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите текст ответа")]
        public string Text { get; set; }
        public bool IsTrueAnswer { get; set; }
        public string TrueAnswer { get; set; }
        public int QuestionId { get; set; }
        public Answers ConvertToAnswer()
        {
            return new Answers
            {
                Id = Id,
                IsTrueAnswer = IsTrueAnswer,
                QuestionId = QuestionId,
                Text = Text
            };
        }

        public static bool ConvertCheckResultToBool(string result)
        {
            return result == "on";
        }
    }

}