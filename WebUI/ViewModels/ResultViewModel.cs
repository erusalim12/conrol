﻿using System;
using System.Collections.Generic;
using Domain.Entity;


namespace WebUI.ViewModels
{
    public class ResultViewModel
    {
        public ResultViewModel()
        {
            Results = new List<Results>();
        }

        public ResultViewModel(Result result)
        {
            Mark = result.Mark;
            QuestionCount = result.QuestionCount;
            ResultInPercent = result.ResultInPercent;
            RightAnswerCount = result.RightAnswerCount;
            SpentTime = result.SpentTime;
            TimeToFill = result.TimeToFill;
            Results = new List<Results>();
        }
        public int Mark { get; set; }

        public int QuestionCount { get; set; }
        public float ResultInPercent { get; set; }

        public int RightAnswerCount { get; set; }

        public TimeSpan SpentTime { get; set; }

        public TimeSpan TimeToFill { get; set; }

        public List<Results> Results { get; set; }
    }

    public class Results
    {
        public int QuestionId { get; set; }
        public int[] AnswersId { get; set; }
    }
}