﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.ViewModels
{
    public class Quiz
    {
        public class QuizAnswers
        {
            public QuizAnswers()
            {
                this.Answers = new List<QuestionAnswer>();
            }

            public int QuizID { get; set; }
            public IList<QuestionAnswer> Answers { get; set; }
            public string FilledTime { get; set; }
        }

        public class QuestionAnswer
        {
            public int QuestionId { get; set; }
            public int[] AnswerId { get; set; }
        }
    }
}