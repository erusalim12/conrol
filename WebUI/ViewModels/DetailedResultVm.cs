﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Controllers;

namespace WebUI.ViewModels
{
    /// <summary>
    /// Отображение результатов теста
    /// </summary>
    public class DetailedResultVm
    {
        public DetailedResultVm()
        {
            RigthAnswers = new List<string>();
            CurrentAnswers = new List<string>();
        }
        /// <summary>
        /// Вопрос
        /// </summary>
        public string Question { get; set; }
        /// <summary>
        /// Правильный ответ
        /// </summary>
        public List<string>  RigthAnswers { get; set; }
        /// <summary>
        /// Ответ участника
        /// </summary>
        public List<string> CurrentAnswers { get; set; }
        /// <summary>
        /// отражает на представлении верно или нет ответил курсант
        /// </summary>
        public bool IsTrueAnswer { get; set; }

        /// <summary>
        /// Пояснение к вопросу
        /// </summary>
        public string Comment { get; set; }
    }

}