﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.ViewModels
{
    public class PassingGroupsVm
    {

        public int TestId { get; set; }
        public int[] GropList { get; set; }
        public string IsOpen { get; set; }
    }
}