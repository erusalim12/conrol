﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Entity;

namespace WebUI.ViewModels.Interfaces
{
    public interface ITestCreate
    {
        [Display(Name = "Отлично")]
        [Required]
        byte Perfect { get; set; }

        [Display(Name = "Хорошо")]
        [Required]
        byte Good { get; set; }

        [Display(Name = "Удовлетворительно")]
        [Required]
        byte Satisfactorily { get; set; }

        string Name { get; set; }
        bool IsOpenTest { get; set; }

        TimeSpan TimeToExecute { get; set; } 
        int Id { get; set; }

        [Display(Name = "Размер выборки")]
        byte FetchSize { get; set;}
        Test ConvertToTest();
        int CategoryId { get; set; }
        string Comment { get; set; }

    }
}