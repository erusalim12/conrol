﻿using System.Collections.Generic;

namespace WebUI.ViewModels.Interfaces
{
     public interface ITestDetails : ITestCreate
    {
        string TeacherId { get; set; }
        string AddingDate { get; set; }
        List<QuestionViewModel> Questions { get; set; }
        string NumsOfPassedGroup { get; set; }
    }

}