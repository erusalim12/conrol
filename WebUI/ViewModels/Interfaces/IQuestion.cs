﻿using System.ComponentModel.DataAnnotations;
using Domain.Entity;

namespace WebUI.ViewModels
{
    public interface IQuestion
    {
        [DataType(DataType.MultilineText)]
        string Question { get; set; }
        string Comment { get; set; }
        int Id { get; set; }
        int TestId { get; set; }

        Questions ConvertToQuestion();
    }
}