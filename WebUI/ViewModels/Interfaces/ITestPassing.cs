﻿
using System.Collections.Generic;

namespace WebUI.ViewModels.Interfaces
{
    public interface ITestPassing
    {
    string Name { get; set; }
    int Id { get; set; }
    List<QuestionViewModel> Questions { get; set; }

    }
}