﻿using System;

namespace WebUI.ViewModels
{
    public interface IShowTests
    {
        string AddingDate { get; set; }
        int Id { get; set; }
        string Name { get; set; }
        string TeacherName { get; set; }
        TimeSpan TimeToExecute { get; set; }
        bool IsPassed { get; set; }
        string Comment { get; set; }
    }
}