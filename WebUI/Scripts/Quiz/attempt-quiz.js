﻿$('#submitButton').click(function () {
    $("#testWindow").prop("disabled", true);  
            var answers = [];
            var count = 1;

            $(".question").each(function () {

                var selected = [];
                $("input:checkbox[name='question " + count + "']:checked").each(function () {
                    selected.push($(this).val());
                });
                answers.push({
                    QuestionID: $(this).attr("id"),
                    //вытащить из div все checked
                    AnswerID: selected
                });

                count++;
            });
            var quizAnswers = {
                QuizID: $(".page-header").attr("id"),
                FilledTime : $('#time').text(),
                Answers : answers
            };

            for(var i = 0; i < quizAnswers.Answers.length; i++) {
                if (quizAnswers.Answers[i].AnswerID === undefined) {
                    $("#quizAlert").slideDown();
                    return;
                }
            }
            SendData(quizAnswers);

        });

function SendData(data) {
    $.ajax({
        type: "POST",
        url: "/TestPassing/Passing/",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (Id) {
            window.location.pathname = "/TestPassing/Results/" + Id;
        },
        error: function (data) {
            alert("Ошибка " + data.responseJSON);
        }
    });
}

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            $("#submitButton").click();
            return;
        }
    }, 1000);
}

