﻿using Domain.Abstract;
using Domain.Entity;
using Domain.Repository;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        /// <summary>
        /// Привязки Ninject
        /// </summary>
        private void AddBindings()
        {
            kernel.Bind<ITemplateRepository<Answers>>().To<AnswersRepository>();
            kernel.Bind<ITemplateRepository<Cathedras>>().To<CathedraRepository>();
            kernel.Bind<ITemplateRepository<CursantAnswerList>>().To<CursantAnswerListRepository>();
            kernel.Bind<ITemplateRepository<Cursants>>().To<CursantsRepository>();
            kernel.Bind<ITemplateRepository<CursantTestPassing>>().To<CursantTestPassingRepository>();
            kernel.Bind<ITemplateRepository<Groups>>().To<GroupsRepository>();
            kernel.Bind<ITemplateRepository<Questions>>().To<QuestionsRepository>();
            kernel.Bind<ITemplateRepository<Teachers>>().To<TeachersRepository>();
            kernel.Bind<ITemplateRepository<Test>>().To<TestRepository>();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
    }
}
