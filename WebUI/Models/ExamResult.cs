﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    /// <summary>
    /// Представляет Результат прохождения теста
    /// </summary>
    public class ExamResult
    {
        /// <summary>
        /// Оценка
        /// </summary>
        public byte Mark { get; set; }
        /// <summary>
        /// Количество правильных ответов
        /// </summary>
        public byte RigthAnswers { get; set; }  
        /// <summary>
        /// количество вопросов
        /// </summary>
        public byte QuestCount { get; set; }
        /// <summary>
        /// результат в процентах
        /// </summary>
        public float ResultInPercent { get; set; }

    }
}