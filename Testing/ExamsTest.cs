﻿using Domain.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Controllers;
using WebUI.ViewModels;

namespace Testing
{
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public class ExamsTest
    {
        private EfDbContext _db = new EfDbContext();
        [TestMethod]
        public void CreateTest()
        {
            var controller = new ExamController();
            ITestCreate added_Exam = new TestViewModel
            {
                Name = "test.Test",
                Good = 75,
                IsOpenTest = true,
                TimeToExecute = TimeSpan.FromMinutes(90),
                Perfect = 85,
                Satisfactorily = 60
            };

            string expected = "Create";
            ViewResult result = controller.Create(added_Exam) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);
        }
        [TestMethod]
        public void CreateViewResultNotNull()
        {
            ExamController controller = new ExamController();

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }
    }
 
}
