﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Domain.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Fio { get; set; }
        public int? Cafedra { get; set; }
        public int? Facultet { get; set; }
        public int? GroupNum { get; set; }

        public ApplicationUser()
        {
        }
    }
}

