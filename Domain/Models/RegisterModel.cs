﻿using System.ComponentModel.DataAnnotations;

namespace WebUI.Models
{
    public class RegisterModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Fio { get; set; }

        [Required]
        public string RoleId { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }

        [Display(Name = "Кафедра")]
        [Range(1, 15, ErrorMessage = "Введите корректный номер")]
        public int? Cafedra { get; set; }

        [Display(Name = "Факультет")]
        [Range(1, 15, ErrorMessage = "Введите корректный номер")]
        public int? Facultet { get; set; }

        [Display(Name = "Группа")]
        [Range(111, 9555, ErrorMessage = "Введите корректный номер")]
        public int? GroupNum { get; set; }
    }
}