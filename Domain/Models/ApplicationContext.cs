﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entity;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Domain.Models
{

    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext() : base("AuthConnection") { }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }

    }

    public class AppContextInitializer : CreateDatabaseIfNotExists<ApplicationContext>
    {
        protected override void Seed(ApplicationContext context)
        {
           
            context.Roles.Add(new ApplicationRole
            {
                Name = "Преподаватель"
              
            });
            context.Roles.Add(new ApplicationRole
            {
                Name = "Курсант"
            });

            context.Roles.Add(new ApplicationRole
            {
                Name = "Администратор"
            });
            context.SaveChanges();

            base.Seed(context);
        }
    }
}