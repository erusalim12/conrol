namespace Domain.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CursantAnswerList")]
    public partial class CursantAnswerList
    {
        public int Id { get; set; }

        public int TestPassingId { get; set; }

        public int QuestionId { get; set; }

        public int AnswerId { get; set; }


        public virtual Answers Answers { get; set; }

        public virtual CursantTestPassing CursantTestPassing { get; set; }

        public virtual Questions Questions { get; set; }
    }
}
