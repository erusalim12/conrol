namespace Domain.Entity
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EfDbContext : DbContext
    {
        public EfDbContext()
            : base("name=DefaultConnection")
        {
        }

        public virtual DbSet<TestCategory> TestCategories { get; set; }
        public virtual DbSet<Answers> Answers { get; set; }
        public virtual DbSet<Cathedras> Cathedras { get; set; }
        public virtual DbSet<CursantAnswerList> CursantAnswerList { get; set; }
        public virtual DbSet<Cursants> Cursants { get; set; }
        public virtual DbSet<CursantTestPassing> CursantTestPassing { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
        public virtual DbSet<Questions> Questions { get; set; }
        public virtual DbSet<Teachers> Teachers { get; set; }
        public virtual DbSet<Test> Test { get; set; }
        public virtual DbSet<Result> Result { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<TestCategory>()
            //    .HasMany(t => t.Tests)
            //    .WithOptional(t => t.Category)
            //    .HasForeignKey(e => e.CategoryId)
            //    .WillCascadeOnDelete(true);


            modelBuilder.Entity<Answers>()
                .HasMany(e => e.CursantAnswerList)
                .WithRequired(e => e.Answers)
                .HasForeignKey(e => e.AnswerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cathedras>()
                .HasMany(e => e.Teachers)
                .WithRequired(e => e.Cathedras)
                .HasForeignKey(e => e.CathedraId);

            //modelBuilder.Entity<Cursants>()
            //    .HasMany(e => e.CursantTestPassing)
            //    .WithMany(e => e.Cursants)
            //    .Map(m => m.ToTable("Cursant_CursantTestPassing").MapLeftKey("CursantId").MapRightKey("TestId"));

            modelBuilder.Entity<CursantTestPassing>()
                .HasMany(e => e.CursantAnswerList)
                .WithRequired(e => e.CursantTestPassing)
                .HasForeignKey(e => e.TestPassingId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Groups>()
                .HasMany(e => e.Cursants)
                .WithRequired(e => e.Groups)
                .HasForeignKey(e => e.GroupId);

            modelBuilder.Entity<Questions>()
                .HasMany(e => e.Answers)
                .WithRequired(e => e.Questions)
                .HasForeignKey(e => e.QuestionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Questions>()
                .HasMany(e => e.CursantAnswerList)
                .WithRequired(e => e.Questions)
                .HasForeignKey(e => e.QuestionId);




        }

    }
}
