namespace Domain.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TestPassing")]
    public partial class TestPassing
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TestPassing()
        {
        }

        public int Id { get; set; }

        public int TestId { get; set; }

        public TimeSpan TimeCount { get; set; }

        public byte Result { get; set; }

        public string PassingDate { get; set; }


        public virtual Test Test { get; set; }

   

        public string CursantId { get; set; }

    }
}
