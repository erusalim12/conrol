namespace Domain.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Test")]
    public partial class Test
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Test()
        {
            CursantTestPassing = new HashSet<CursantTestPassing>();
            PassedGroups = new HashSet<Groups>();
            Questions = new HashSet<Questions>();
            AddingDate = DateTime.Now.ToShortDateString();
        }
        [Key]
        public int Id { get; set; }

        [StringLength(128)]
       
        public string TeacherId { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public bool IsOpenTest { get; set; }

        public string AddingDate { get; private set; }

        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = @"{0:mm\:ss}")]
        public TimeSpan TimeToExecute { get; set; }

        [Display(Name = "������ �������")]
        public byte FetchSize { get; set; }

        //�����������
        public byte Perfect { get; set; }
 
        public byte Good { get; set; }
 
        public byte Satisfactorily { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CursantTestPassing> CursantTestPassing { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Groups> PassedGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Questions> Questions { get; set; }
        public virtual TestCategory Category { get; set; }
        
        public int CategoryId { get; set; }
        public string Comment { get; set; }

    }
}
