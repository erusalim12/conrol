namespace Domain.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Answers
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Answers()
        {
            CursantAnswerList = new HashSet<CursantAnswerList>();
        }

        public int Id { get; set; }

        public int QuestionId { get; set; }

        [Required]
        public string Text { get; set; }

        public bool IsTrueAnswer { get; set; }

        public virtual Questions Questions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CursantAnswerList> CursantAnswerList { get; set; }
    }
}
