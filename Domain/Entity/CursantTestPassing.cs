namespace Domain.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CursantTestPassing")]
    public partial class CursantTestPassing
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CursantTestPassing()
        {
            CursantAnswerList = new HashSet<CursantAnswerList>();
        }

        public int Id { get; set; }

        public int TestId { get; set; }

        public TimeSpan TimeCount { get; set; }

        public byte Result { get; set; }

        public string PassingDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CursantAnswerList> CursantAnswerList { get; set; }

        public virtual Test Test { get; set; }

   
        public virtual Cursants Cursant { get; set; }
        public string CursantId { get; set; }

    }
}
