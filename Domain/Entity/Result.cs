﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entity
{
  public class Result
    {
        public Result()
        {
            Date = DateTime.Now.ToShortDateString();
        }
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Date { get; }
        public float ResultInPercent { get; set; }
        public byte Mark { get; set; }
        public byte QuestionCount { get; set; }
        public byte RightAnswerCount { get; set; }
        /// <summary>
        /// Время, затраченное на прохождение теста
        /// </summary>
        public TimeSpan SpentTime { get; set; }

        /// <summary>
        /// Время на прохождение теста
        /// </summary>
        public TimeSpan TimeToFill { get; set; }

        public int TestId { get; set; }

    }
}
