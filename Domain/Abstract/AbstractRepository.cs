﻿using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public abstract class AbstractRepository<T>: IDisposable,ITemplateRepository<T>where T:class
    {
        protected EfDbContext _db = new EfDbContext();
        public IEnumerable<T> ItemCollection
        {
            get { return (IEnumerable<T>)_db.Set<T>(); }
        }
        public virtual void Create(T item)
        {
            _db.Set<T>().Add(item);
            _db.SaveChanges();
        }

        public virtual void Update(T item)
        {
            _db.Entry(item).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public T Details(int id)
        {
            return _db.Set<T>().Find(id);
        }

        public void Delete(T item)
        { 
            _db.Set<T>().Remove(item);
            _db.SaveChanges();
        }
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
