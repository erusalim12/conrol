﻿using Domain.Abstract;
using Domain.Entity;

namespace Domain.Repository
{
    public class QuestionsRepository : AbstractRepository<Questions> { }
}