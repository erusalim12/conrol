﻿using Domain.Abstract;
using Domain.Entity;

namespace Domain.Repository
{
    public class CursantsRepository : AbstractRepository<Cursants> { }
}