﻿using Domain.Abstract;
using Domain.Entity;
using System;

namespace Domain.Repository
{
    public class AnswersRepository : AbstractRepository<Answers>    { }
    public class CathedraRepository : AbstractRepository<Cathedras> { }
    public class CursantAnswerListRepository : AbstractRepository<CursantAnswerList> { }
    public class CursantsRepository : AbstractRepository<Cursants> { }
    public class CursantTestPassingRepository : AbstractRepository<CursantTestPassing> { }
    public class GroupsRepository : AbstractRepository<Groups> { }
    public class QuestionsRepository : AbstractRepository<Questions> { }
    public class TeachersRepository : AbstractRepository<Teachers> { }
    public class TestRepository : AbstractRepository<Test> { }
}
