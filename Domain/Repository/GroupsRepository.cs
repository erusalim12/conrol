﻿using Domain.Abstract;
using Domain.Entity;

namespace Domain.Repository
{
    public class GroupsRepository : AbstractRepository<Groups> { }
}