﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI2.Controllers
{
    public class ExamineController : Controller
    {
        // GET: Examine
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Create(QuizViewModel.Quiz data)
        {
            var quiz = this.MapQuiz(data);
            _context.Quizzes.Add(quiz);
            _context.SaveChanges();

            return null;
        }
    }
}